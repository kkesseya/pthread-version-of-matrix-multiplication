/*Name: Kwaku Kessey-Ankomah Jr
Assignment 1
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


void setMatrixA_Dimen();
void setMatrixB_Dimen();
void setMatrixC_Dimen();
void fillMatrixA();
void fillMatrixB();
void validateNumThreadsArg();
void *multiRoutine(void *threadInfo);


int numPThreads = 1; //default value for # Pthreads used
char *line = NULL; //ptr for stdin
int dimensionA[2], dimensionB[2], c, bytesread; //dimensions for matrix A and B
size_t len = 0;
int ** matrixC; //declare global 2d array matrixC
int ** matrixB; //declare global 2d array matrixB
int ** matrixA; //declare global 2d array matrixA

struct threadInfoStruct {
/*	int numRows;  //num row for matrixA
	int rowLength;
	int numCols; //num columns for matrixB
	int numThreads; //total num of threads*/
	int startRow; //starting point(row) for matrix A 
};


int main (int argc, char *argv[])
{
	//does a quantity validation check on arguments provided	
	if (argc != 2) {
		fprintf(stderr, "num of pThreads and input matrix are required as arguments\n");
		exit(1);
	}


	numPThreads = atoi(argv[1]);

	setMatrixA_Dimen(); //get matrixA's dimensions 

	validateNumThreadsArg();

	fillMatrixA(); //fill matrixA from values from stdin 
	setMatrixB_Dimen();
	fillMatrixB();
	setMatrixC_Dimen();
	free(line);


	pthread_t threads[numPThreads]; //declaring pThreads

    for (int i = 0; i < numPThreads; i++) { 
        struct threadInfoStruct *threadInfo = (struct threadInfoStruct *) malloc(sizeof(struct threadInfoStruct));

        //specifying pThread attributes
        // threadInfo->numRows = dimensionA[0];
        // threadInfo->numCols = dimensionB[1];
        // threadInfo->numThreads = numPThreads;
        threadInfo->startRow = i;
		// threadInfo->rowLength = dimensionA[1];

        pthread_attr_t attr; //attributes
        pthread_attr_init(&attr); //default attributes
        pthread_create(&threads[i], &attr, multiRoutine, threadInfo); 
    } 
     // joining and waiting for all threads to complete 
    for (int i = 0; i < numPThreads; i++){
        pthread_join(threads[i], NULL);
    }

    int r = 0;
    int d = 0;
    for(d; d<dimensionA[0]; d++){
    	for(r=0;r<dimensionB[1];r++){
    			printf("%d  ",matrixC[d][r]);
    	}
    	printf("\n");
    }	
	free(matrixA);
	free(matrixB);
	free(matrixC);
    return 0;
}

void *multiRoutine(void *threadInfo) {

	struct threadInfoStruct *data = threadInfo; // the structure that holds our data

	int n, sum = 0; //the counter and sum
	int v;
	int k; //the starting row for this thread
	for(k= data->startRow; k<dimensionA[0];k = k+numPThreads){//rows in Matrix A
		for(v = 0; v < dimensionB[1]; v++){//columns in Matrix B
			//Row multiplied by column
			sum = 0;
			for(n = 0; n<dimensionA[1]; n++){
				sum += matrixA[k][n] * matrixB[n][v];
			}
			matrixC[k][v] = sum; //sum to its coordinates
		}
	}

	//Exit the thread
	pthread_exit(0);
}


void fillMatrixA(){
	char* input1;

    int rowNum = 0;
    int colNum = 0;
	while (getline(&line, &len, stdin) != -1) {
		input1 = line;
		colNum = 0;
	    while (sscanf(input1, "%d%n", &c, &bytesread) > 0) {
	        matrixA[rowNum][colNum++] = c;
	        input1 += bytesread;
	    }

		if(rowNum >= (dimensionA[0]-1)){
			break;//array a has been filled
		}
		rowNum++; //file lineNum

	}

}

void fillMatrixB(){
	char* input1;

    int rowNum = 0;
    int colNum = 0;
	while (getline(&line, &len, stdin) != -1) {
		
		input1 = line;
		colNum = 0;
	    while (sscanf(input1, "%d%n", &c, &bytesread) > 0) {
	        matrixB[rowNum][colNum++] = c;
	        input1 += bytesread;
	    }

		if(rowNum >= (dimensionB[0]-1)){
			break;//array a has been filled
		}
		rowNum++; //file lineNum

	}

}

void setMatrixA_Dimen(){
	getline(&line, &len, stdin);

	char* input1 = line;
	int length = 0;
    while (sscanf(input1, "%d%n", &c, &bytesread) > 0) {
        dimensionA[length++] = c;
        input1 += bytesread;
    }

    matrixA = malloc(sizeof(int *)*dimensionA[0]);
	for(int i = 0; i<dimensionA[0]; i++){
		matrixA[i] = malloc(sizeof(int)*dimensionA[1]);
	}
	
}

void setMatrixB_Dimen(){
	getline(&line, &len, stdin);

	char* input1 = line;
	int length = 0;
    while (sscanf(input1, "%d%n", &c, &bytesread) > 0) {
        dimensionB[length++] = c;
        input1 += bytesread;
    }

    if(dimensionA[1] != dimensionB[0]){
    	fprintf(stderr, "Dimensions of matrix A and B are incompatible\n");
    	free(line);
		exit(1);
    }

    matrixB = malloc(sizeof(int *)*dimensionB[0]);
	for(int i = 0; i<dimensionB[0]; i++){
		matrixB[i] = malloc(sizeof(int)*dimensionB[1]);
	}
	
}

void setMatrixC_Dimen(){
    matrixC = malloc(sizeof(int *)*dimensionA[0]);
	for(int i = 0; i<dimensionA[0]; i++){
		matrixC[i] = malloc(sizeof(int)*dimensionB[1]);
	}
}

void validateNumThreadsArg(){
	if(numPThreads > dimensionA[0]){ //validate the amount of pThreads specified
		fprintf(stderr, "Number of PThreads can not exceed %d threads(limiting factor is MAtrixA's max rows).\n", dimensionA[0]);
		free(line);
		exit(1);
	}
}
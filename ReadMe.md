Name: Kwaku Kessey-Ankomah Jr
Assignment: 1

My program is able to accept matrices of any size as long as they abide by normal matrix multiplcation rules


Format of matrix file:
	Example matrx files is provided: 200x200X200x200.txt , 2x4X4x4.txt , 2x2X2x3.txt, 4000x4000X4000x4000.txt 
	Structure of matrix file:
	
    2 4                  dimensions for matrixA
    1 2 3 4
    3 2 4 5
    4 4                  dimensions for matrixB
    2 4 1 5
    1 2 3 4
    3 2 4 5
    9 3 2 3
	


Instructions:
	Compilation:
		`gcc -pthread -o a1 a1.c`
	Execution(Running):
		`./a1 2 < inputMatrices.txt > outputMatrix.txt`

Results:
```
    (for 200x200X200x200):
		1 Threads:
			real	0m0.039s
			user	0m0.039s
			sys	0m0.000s
		2 Threads:
			real	0m0.026s
			user	0m0.039s
			sys	0m0.000s
		3 Threads:
			real	0m0.023s
			user	0m0.039s
			sys	0m0.000s
		4 Threads:
			real	0m0.023s
			user	0m0.039s
			sys	0m0.000s
		6 Threads:
			real	0m0.022s
			user	0m0.035s
			sys	0m0.004s

	(for 4000x4000X4000x4000):
		1 Threads:
			real	12m55.832s
			user	12m55.197s
			sys	0m0.240s
		2 Threads:
			real	6m32.160s
			user	12m56.626s
			sys	0m0.240s
		3 Threads:
			real	4m28.217s
			user	12m56.139s
			sys	0m0.808s
		4 Threads:
			real	3m44.456s
			user	12m51.508s
			sys	0m1.167s
		6 Threads:
			real	3m35.517s
			user	12m50.964s
			sys	0m0.944s
```
Analyses:  As you can see in the results the more we parallelised the matrix multiplication problem the faster it executes. For the 4000x4000X4000x4000 multiplication as we increased the pThread count the execution time halved for each additional thread. Another interesting data point is how the size of the matrix affects the effectiveness of adding more pThreads to the problem. For instance the 200x200 problem barely gained significant boosts in execution time when more threads were added. However, when a larger data set 4000x4000 was given the benefits of having multiple threads were more noticable. Within my test I also tried to allocate more threads than available cores to my program to see its effect, and it resulted in improved and faster execution time. This is because if the amount of pThreads for the program increase pass the core count we can essentially fill the job scheduler with our pThread processes. Resulting in more cycles allocated to the matrix multiplication program as a whole. However, it is to be noted that the gains will not be as high as the pThreads<=coreCount.


